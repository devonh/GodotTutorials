#include "Player.h"

using namespace godot;

void Player::_register_methods()
{
    register_method("_ready", &Player::_ready);
    register_method("_init", &Player::_init);
}

Player::Player()
{

}

Player::~Player()
{

}

void Player::_init()
{

}

void Player::_ready()
{
    Godot::print("Hello World!");
}

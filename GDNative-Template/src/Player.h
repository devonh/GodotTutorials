#pragma once

#include <Godot.hpp>
#include <KinematicBody.hpp>

namespace godot
{

class Player : public KinematicBody
{
    GODOT_CLASS(Player, KinematicBody)

public:
    static void _register_methods();

    Player();
    ~Player();

    void _init();
    void _ready();

};

}
